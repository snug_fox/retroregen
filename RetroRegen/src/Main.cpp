#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "opengl32.lib")

#include "display\Display.h"
#include "display\Mouse.h"
#include "timing\Timing.h"
#include "pong/Pong.h"
#include <iostream>

#define WINDOWS_MEAN_AND_LEAN

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	Display::create(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
	Pong pong;
	Ball ball(&pong, 395, 235);
	KeyBindings playerCont1 = { 0x57, 0x53 };
	KeyBindings playerCont2 = { VK_UP, VK_DOWN };
	Player player1(playerCont1);
	Player player2(playerCont2);
	Computer comp1(&ball, COMP_HARD);
	Computer comp2(&ball, COMP_MED);
	Paddle lPaddle(&player1, 10);
	Paddle *rPaddle;
	if (MessageBox(NULL, "Play with a computer?", "Choose Competitor", MB_YESNO | MB_ICONQUESTION) == IDYES)
		if (MessageBox(NULL, "Want a challenge?", "Choose Level", MB_YESNO | MB_ICONQUESTION) == IDYES)
			rPaddle = new Paddle(&comp1, 770);
		else
			rPaddle = new Paddle(&comp2, 770);
	else
		rPaddle = new Paddle(&player2, 770);
	pong.setElements(&ball, &lPaddle, rPaddle);
	while (!Display::close_requested) {
		sync(60);
		pong.update(16);
		Display::update();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	DestroyWindow(Display::hWnd);
	delete rPaddle;
	return 0;
}