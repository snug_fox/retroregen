#pragma once
#include <Windows.h>
#include <gl\GL.h>
#include "Color.h"
#include "GLFPOINT.h"

class GLQuad
{
public:
	GLQuad(GLFPOINT* point, GLint* width, GLint* height, Color *color);
	~GLQuad();
	void draw();
private:
	GLFPOINT *pt;
	GLint *w, *h;
	Color *color;
};

