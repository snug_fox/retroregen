#include "GLQuad.h"

GLQuad::GLQuad(GLFPOINT* point, GLint* width, GLint* height, Color *color)
{
	pt = point;
	w = width;
	h = height;
	this->color = color;
}

GLQuad::~GLQuad()
{
}

void GLQuad::draw()
{
	glColor3f(color->r, color->g, color->b);
	glBegin(GL_QUADS);
	glVertex2f(pt->x, pt->y);
	glVertex2f(pt->x + *w, pt->y);
	glVertex2f(pt->x + *w, pt->y + *h);
	glVertex2f(pt->x, pt->y + *h);
	glEnd();
}
