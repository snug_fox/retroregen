#include "SyncAvg.h"
#include "Timing.h"
#include <Windows.h>

long nextFrame(0);
bool initialized(false);
SyncAvg sleepDurations(10);
SyncAvg yieldDurations(10);

void initialize()
{
	initialized = true;

	sleepDurations.init(1);
	yieldDurations.init((int)(-(getTime() - getTime()) * 1.333));

	nextFrame = getTime();
}

void sync(short fps)
{
	if (fps <= 0) return;
	if (!initialized) initialize();

	for (long t0 = getTime(), t1; nextFrame - t0 > sleepDurations.avg(); t0 = t1)
	{
		Sleep(1);
		sleepDurations.add((t1 = getTime()) - t0);
	}
	sleepDurations.dampenForLowResTicker();
	for (long t0 = getTime(), t1; (nextFrame - t0) > yieldDurations.avg(); t0 = t1)
	{
		Yield();
		yieldDurations.add((t1 = getTime()) - 10);
	}

	nextFrame = max(nextFrame + MILLIS_IN_SECOND / fps, getTime());
}
