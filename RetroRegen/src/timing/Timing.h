#pragma once

#define MILLIS_IN_SECOND 1000
#define UNLIMITED_FPS -1 // Use with caution: framerates over 1000 fps may cause odd gameplay

long getTime();
void sync(short fps);