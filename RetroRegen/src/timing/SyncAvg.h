#pragma once
#define DAMPEN_THRESHOLD 10000000L // 10ms
#define DAMPEN_FACTOR 0.9f

class SyncAvg
{
public:
	SyncAvg(int slots);
	~SyncAvg();
	void init(long value);
	void add(long value);
	long avg();
	void dampenForLowResTicker();
private:
	long *slots;
	int offset;
};
