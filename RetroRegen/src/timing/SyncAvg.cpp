#include "SyncAvg.h"

SyncAvg::SyncAvg(int slots)
{
	SyncAvg::slots = new long[slots];
	offset = 0;
}

SyncAvg::~SyncAvg()
{
	delete[] slots;
}

void SyncAvg::init(long value)
{
	while (offset < sizeof(slots))
		slots[offset++] = value;
}

void SyncAvg::add(long value)
{
	slots[offset++ % sizeof(slots)] = value;
	offset %= sizeof(slots);
}

long SyncAvg::avg()
{
	long sum = 0;
	for (int i = 0; i < sizeof(slots); i++)
		sum += slots[i];
	return sum / sizeof(slots);
}

void SyncAvg::dampenForLowResTicker()
{
	if (avg() > DAMPEN_THRESHOLD)
	for (int i = 0; i < sizeof(slots); i++)
		slots[i] = (long)(slots[i] * DAMPEN_FACTOR);
}
