#include "Mouse.h"

POINT mousePos = { 0, 0 };

void setMousePos(int x, int y)
{
	mousePos.x = x;
	mousePos.y = y;
}

int Mouse::getMousePos(int measure)
{
	if (measure == MP_X)
		return mousePos.x;
	else if (measure == MP_Y)
		return mousePos.y;
	return -1;
}

bool Mouse::procMouseMsgs(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CAPTURECHANGED: // Don't touch, very finnicky
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_LBUTTONUP:
		break;

	case WM_MBUTTONDOWN:
		break;

	case WM_MBUTTONUP:
		break;

	case WM_MOUSEACTIVATE:
		break;

	case WM_MOUSEHOVER:
		break;

	case WM_MOUSEHWHEEL:
		break;

	case WM_MOUSELEAVE:
		break;

	case WM_MOUSEMOVE:
		setMousePos(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_MOUSEWHEEL:
		break;

	default:
		return false;
	}

	return true;
}
