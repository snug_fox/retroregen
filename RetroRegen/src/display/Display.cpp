#include "Display.h"
#include "Mouse.h"

CHAR	appName[] = "RetroRegen";
HWND	Display::hWnd;
HDC		hDC;
HGLRC	hRC;
MSG		msg;
bool	Display::close_requested = false;

bool setupPixelFormat(HDC);
GLvoid resize(GLsizei, GLsizei);
GLvoid initGL(GLsizei, GLsizei);
LONG WINAPI MainWndProc(HWND, UINT, WPARAM, LPARAM);


int WINAPI Display::create(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASS wndclass;

	wndclass.style = NULL;
	wndclass.lpfnWndProc = (WNDPROC)MainWndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = NULL;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = appName;

	if (!RegisterClass(&wndclass))
		return false;

	/* Create the frame */
	hWnd = CreateWindow(appName,
		appName,
		WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		800,
		480,
		NULL,
		NULL,
		hInstance,
		NULL);

	/* make sure window was created */
	if (!hWnd)
		return false;

	/* show and update main window */
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return true;
}

void processMessages()
{
	while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
	{
		if (GetMessage(&msg, NULL, 0, 0))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			return;
		}
	}
}

void Display::update(void)
{
	processMessages();
	SwapBuffers(hDC);
}

/* main window procedure */
LONG WINAPI MainWndProc(
	HWND    hWnd,
	UINT    uMsg,
	WPARAM  wParam,
	LPARAM  lParam)
{
	LONG    lRet = 1;
	PAINTSTRUCT    ps;
	RECT rect;

	switch (uMsg) {

	case WM_CREATE:
		hDC = GetDC(hWnd);
		if (!setupPixelFormat(hDC))
			PostQuitMessage(0);

		hRC = wglCreateContext(hDC);
		wglMakeCurrent(hDC, hRC);
		GetClientRect(hWnd, &rect);
		initGL(rect.right, rect.bottom);
		break;

	case WM_PAINT:
		BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;

	case WM_SIZE:
		GetClientRect(hWnd, &rect);
		resize(rect.right, rect.bottom);
		break;

	case WM_CLOSE:
		Display::close_requested = true;
		break;

	case WM_DESTROY:
		if (hRC)
			wglDeleteContext(hRC);
		if (hDC)
			ReleaseDC(hWnd, hDC);
		hRC = NULL;
		hDC = NULL;
		break;

	default:
		if (!Mouse::procMouseMsgs(uMsg, wParam, lParam))
			lRet = DefWindowProc(hWnd, uMsg, wParam, lParam);
		break;
	}

	return lRet;
}

bool setupPixelFormat(HDC hdc)
{
	int pixelformat;
	PIXELFORMATDESCRIPTOR pfd, *ppfd;
	ppfd = &pfd;

	ppfd->nSize = sizeof(PIXELFORMATDESCRIPTOR);
	ppfd->nVersion = 1;
	ppfd->dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	ppfd->dwLayerMask = PFD_MAIN_PLANE;
	ppfd->iPixelType = PFD_TYPE_RGBA;
	ppfd->cColorBits = 16;
	ppfd->cDepthBits = 16;
	ppfd->cAccumBits = 0;
	ppfd->cStencilBits = 0;

	pixelformat = ChoosePixelFormat(hdc, ppfd);

	if (pixelformat == 0)
	{
		MessageBox(NULL, "ChoosePixelFormat unsuccessful.", "Error", MB_OK);
		return false;
	}

	if (!SetPixelFormat(hdc, pixelformat, ppfd))
	{
		MessageBox(NULL, "SetPixelFormat unsuccessful.", "Error", MB_OK);
		return false;
	}

	return true;
}

GLvoid resize(GLsizei width, GLsizei height)
{
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);
}

GLvoid initGL(GLsizei width, GLsizei height)
{
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	resize(width, height);
}
