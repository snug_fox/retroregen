#pragma once
#include <Windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>

class Display
{
public:
	static int WINAPI create(HINSTANCE hInstance, HINSTANCE hPrevInstence, LPSTR lpCmdLine, int nCmdShow);
	static GLvoid update();
	static HWND hWnd;
	static bool close_requested;
private:
	Display();
	~Display();
};
