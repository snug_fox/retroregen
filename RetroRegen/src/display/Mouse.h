#pragma once
#include <Windows.h>

#define MB_LBUTTON 0
#define MB_MBUTTON 1
#define MB_RBUTTON 2
#define MP_X 0
#define MP_Y 1

class Mouse
{
private:
	Mouse();
public:
	static int getMousePos(int measure);
	static bool procMouseMsgs(UINT uMsg, WPARAM wParam, LPARAM lParam);
};
