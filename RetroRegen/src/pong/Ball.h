#pragma once
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdlib.h>
#include "../glutil/GLQuad.h"
#include "Pong.h"

#define BALL_MAX_SPEED 200
#define BALL_MIN_SPEED 150

class Pong;

class Ball
{
public:
	Ball(Pong *thePong, int x, int y);
	~Ball();
	GLFPOINT *pos;
	void reflectYAxis();
	float velocity, dir;
	Color *color;
	int width, height;
	float speed_factor;
	void update(long delta);
	void newSpeedDir();
	void randomizeDirection();
private:
	Pong *thePong;
	GLQuad *ball;
	double toRadians(float deg);
};

