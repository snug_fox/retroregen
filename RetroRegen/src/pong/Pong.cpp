#include "Pong.h"

Pong::Pong()
{
	timer = 0;
}

Pong::~Pong()
{
}

void Pong::setElements(Ball *ball, Paddle *lPaddle, Paddle *rPaddle)
{
	this->ball = ball;
	this->lPaddle = lPaddle;
	this->rPaddle = rPaddle;
}

void Pong::update(long delta)
{
	if (scores[0] > 9)
	{
		MessageBox(NULL, "Left Paddle Wins!", "Game Over", MB_OK);
		restart();
	}
	else if (scores[1] > 9)
	{
		MessageBox(NULL, "Right Paddle Wins!", "Game Over", MB_OK);
		restart();
	}
	else
	{

		long applied_delta;
		if (timer > 2000)
			applied_delta = delta;
		else
		{
			timer += delta;
			applied_delta = 0;
		}
		ball->update(applied_delta);
		lPaddle->update(delta);
		rPaddle->update(delta);
	}
}

void Pong::pt(short paddle)
{
	scores[paddle]++;
	updateScores();
	reset();
}

void Pong::updateScores()
{
	std::ostringstream title;
	title << "RetroRegen Alpha: " << scores[0] << " to " << scores[1];
	SetWindowText(Display::hWnd, title.str().c_str());
}

void Pong::reset()
{
	ball->pos->x = 395;
	ball->pos->y = 235;
	lPaddle->pos.y = rPaddle->pos.y = 215;
	wait();
}

void Pong::restart()
{
	scores[0] = scores[1] = 0;
	updateScores();
	reset();
}

void Pong::wait()
{
	timer = 0;
}
