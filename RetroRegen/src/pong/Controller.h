#pragma once
#include "Paddle.h"

#define MOVE_UP 0
#define MOVE_DOWN 1
#define NO_MOVE -1

class Paddle;

class Controller
{
public:
	virtual short getMove() = 0;
	virtual void setPaddle(Paddle *paddle) {};
};
