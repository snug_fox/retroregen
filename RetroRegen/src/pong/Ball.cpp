#include "Ball.h"
#include <time.h>

Ball::Ball(Pong *thePong, int x, int y)
{
	this->thePong = thePong;
	newSpeedDir();
	pos = new GLFPOINT { (GLfloat)x, (GLfloat)y };
	width = height = 10;
	color = new Color { 1.0f, 1.0f, 1.0f };
	ball = new GLQuad(pos, &width, &height, color);
	speed_factor = 1.0f;
}

Ball::~Ball()
{
	delete color, ball, pos;
}

void Ball::reflectYAxis()
{
	if (dir < 0)
		dir = -180 - dir;
	else
		dir = 180 - dir;
}

void Ball::newSpeedDir()
{
	srand((unsigned int)time(0));
	velocity = (float)(rand() % BALL_MAX_SPEED + BALL_MIN_SPEED);
	dir = (float)(rand() % 359);
	while (dir == 180 || dir == 0)
		dir = (float)(rand() % 359);
}

void Ball::update(long delta)
{
	float deltaSec = (float)delta / 1000;
	pos->x += velocity * (float)cos(toRadians(dir)) * deltaSec * speed_factor;
	pos->y += velocity * (float)sin(toRadians(dir)) * deltaSec * speed_factor;
	if (pos->y < 0)
	{
		dir = -dir;
		pos->y = -pos->y;
	}
	else if (pos->y > 439)
	{
		dir = -dir;
		pos->y = 878 - pos->y;
	}
	if (pos->x - 10 < thePong->lPaddle->pos.x)
	{
		Paddle *lPaddle = thePong->lPaddle;
		if (pos->y > lPaddle->pos.y - 10 && pos->y < lPaddle->pos.y + 50)
		{
			velocity += 25;
			dir = 2 * ((pos->y + 5) - (lPaddle->pos.y + 25));
		}
		else
		{
			thePong->pt(PAD_RIGHT);
			newSpeedDir();
		}
	}
	else if (pos->x + 10 > thePong->rPaddle->pos.x)
	{
		Paddle *rPaddle = thePong->rPaddle;
		if (pos->y > rPaddle->pos.y - 10 && pos->y < rPaddle->pos.y + 50)
		{
			velocity += 25;
			dir = 2 * ((pos->y + 5) - (rPaddle->pos.y + 25));
			reflectYAxis();
		}
		else
		{
			thePong->pt(PAD_LEFT);
			newSpeedDir();
		}
	}
	ball->draw();
}

double Ball::toRadians(float deg)
{
	return (double)deg * M_PI / 180;
}
