#include "Player.h"

Player::Player(KeyBindings kbs)
{
	this->kbs = kbs;
}

Player::~Player()
{
}

short Player::getMove()
{
	if (GetAsyncKeyState(kbs.up))
		return MOVE_UP;
	if (GetAsyncKeyState(kbs.down))
		return MOVE_DOWN;
	return NO_MOVE;
}
