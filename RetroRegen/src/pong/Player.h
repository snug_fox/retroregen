#pragma once
#include "Controller.h"
#include <Windows.h>

struct KeyBindings
{
	int up, down;
};

class Player : public Controller
{
public:
	Player(KeyBindings kbs);
	~Player();
	short getMove();
private:
	KeyBindings kbs;
};
