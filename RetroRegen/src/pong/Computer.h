#pragma once
#include "Controller.h"
#include "Ball.h"
#include "Paddle.h"
#define COMP_EASY 2
#define COMP_MED 3
#define COMP_HARD 4

class Ball;
class Paddle;

class Computer : public Controller
{
public:
	Computer(Ball *theBall, short diff);
	~Computer();
	short getMove();
	void setPaddle(Paddle *paddle);
private:
	Ball *theBall;
	Paddle *myPaddle;
	short diff;
};

