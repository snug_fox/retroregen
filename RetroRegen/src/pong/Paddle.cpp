#include "Paddle.h"

Paddle::Paddle(Controller *control, int x)
{
	pos = { (GLfloat)x, 215 };
	this->control = control;
	control->setPaddle(this);
	width = 10;
	height = 50;
	color = { 1.0f, 1.0f, 1.0f };
	paddle = new GLQuad(&pos, &width, &height, &color);
}

Paddle::~Paddle()
{
	delete paddle;
}

void Paddle::update(long delta)
{
	float deltaSec = (float)delta / 1000;
	if (control->getMove() == MOVE_UP)
		pos.y -= deltaSec * 300;
	else if (control->getMove() == MOVE_DOWN)
		pos.y += deltaSec * 300;
	if (pos.y < 0)
		pos.y = 0;
	else if (pos.y + 50 > 447)
		pos.y = 397;
	paddle->draw();
}
