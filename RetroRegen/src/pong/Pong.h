#pragma once
#include <sstream>
#include "Ball.h"
#include "Controller.h"
#include "Computer.h"
#include "Player.h"
#include "../display/Display.h"
#include "Paddle.h"
#define PAD_LEFT 0
#define PAD_RIGHT 1

class Ball;

class Pong
{
public:
	Pong();
	~Pong();
	Ball *ball;
	Paddle *lPaddle, *rPaddle;
	void update(long delta);
	void setElements(Ball *ball, Paddle *lPaddle, Paddle *rPaddle);
	void pt(short paddle);
	void reset();
	void restart();
	void wait();
	void updateScores();
private:
	short scores[2];
	long timer;
};

