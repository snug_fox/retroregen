#include "Computer.h"


Computer::Computer(Ball *theBall, short diff)
{
	this->theBall = theBall;
	this->myPaddle = myPaddle;
	this->diff = diff;
}


Computer::~Computer()
{
}

void Computer::setPaddle(Paddle *paddle)
{
	myPaddle = paddle;
}

short Computer::getMove()
{
	GLfloat center = myPaddle->pos.y + 25;
	GLfloat b_center = theBall->pos->y + 5;
	if (abs(myPaddle->pos.x - theBall->pos->x) < 120 * diff)
	{
		if (b_center > center)
			return MOVE_DOWN;
		if (b_center < center)
			return MOVE_UP;
	}
	return NO_MOVE;
}
