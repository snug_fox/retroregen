#pragma once
#include "Controller.h"
#include <Windows.h>
#include "../glutil/Color.h"
#include "../glutil/GLQuad.h"
#include "../glutil/GLFPOINT.h"

class Controller;

class Paddle
{
public:
	Paddle(Controller *control, int x);
	~Paddle();
	void update(long delta);
	Color color;
	GLFPOINT pos;
private:
	Controller *control;
	int width, height;
	GLQuad *paddle;
};

